@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            @if (\Session::has('success'))
                <div class="alert alert-success">
                    <ul>
                        @foreach (Session::get('success') as $msg)
                            <li>{!! $msg !!}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if (\Session::has('error'))
                <div class="alert alert-danger">
                    <ul>
                        @foreach (Session::get('error') as $msg)
                            <li>{!! $msg !!}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form action="{{route('config.update','1')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="card">
                    <div class="card-header">Configuración</div>
                    <div class="card-body">
                        <h4 class="mt-0 header-title">Logo</h4>
                        <p class="text-muted mb-3">Sube tu logo, se muestra al inicio de la app.</p>
                        @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                        @endif
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="file" class="dropify" data-max-width="257" name="logo"
                                    data-allowed-file-extensions="jpg png jpeg"
                                        data-max-file-size-preview="1M" 
                                        data-default-file="{{asset($config->logo)}}"
                                />
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-gradient-primary btn-lg" type="submit">GUARDAR</button>
                </div>
                <div class="card">
                    <div class="card-header">Publicidad</div>
                    <div class="card-body">
                        <h4 class="mt-0 header-title"></h4>
                        <p class="text-muted mb-3">Ingrese sus llaves admob.</p>
                        @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                        @endif

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group row"><label for="example-text-input"
                                        class="col-sm-2 col-form-label text-right">Key:</label>
                                    <div class="col-sm-10">
                                    <input class="form-control" type="text" name="admob"  id="admob" value="{{$config->admob}}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-gradient-primary btn-lg" type="submit">GUARDAR</button>
                </div>
                <div class="card">
                    <div class="card-header">App URL</div>
                    <div class="card-body">
                        <h4 class="mt-0 header-title"></h4>
                        <p class="text-muted mb-3">Ingrese su url para la conexión con la app.</p>
                        @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                        @endif

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group row"><label for="example-text-input"
                                        class="col-sm-2 col-form-label text-right">App URL:</label>
                                    <div class="col-sm-10">
                                    <input class="form-control" type="text" name="app_url"  id="admob" value="{{$config->app_url}}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-gradient-primary btn-lg" type="submit">GUARDAR</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection