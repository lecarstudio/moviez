@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                @if (\Session::has('success'))
                    <div class="alert alert-success">
                        <ul>
                            @foreach (Session::get('success') as $msg)
                                <li>{!! $msg !!}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @if (\Session::has('error'))
                    <div class="alert alert-danger">
                        <ul>
                            @foreach (Session::get('error') as $msg)
                                <li>{!! $msg !!}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form action="{{route('push.store')}}" method="POST">
                    @csrf
                    <div class="card">
                        <div class="card-header">Notificaciones Push</div>
                        <div class="card-body">
                            <h4 class="mt-0 header-title"></h4>
                            <p class="text-muted mb-3">Nueva notificación</p>
                            @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                            @endif

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group row"><label for="example-text-input"
                                            class="col-sm-2 col-form-label text-right">Título de la notificación:</label>
                                        <div class="col-sm-10">
                                            <input class="form-control" type="text" name="title"  id="title" >
                                        </div>
                                    </div>
                                    <div class="form-group row"><label for="example-text-input"
                                            class="col-sm-2 col-form-label text-right">Texto de la notificación:</label>
                                        <div class="col-sm-10">
                                            <textarea class="form-control" name="body" id=""  rows="4"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row"><label for="example-text-input"
                                        class="col-sm-2 col-form-label text-right">Enlace Url:</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" type="text" name="url"  id="url" >
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-gradient-primary btn-lg" type="submit">ENVIAR</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection