@extends('layouts.app')

@section('css')
  <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
              @if (\Session::has('success'))
              <div class="alert alert-success">
                  <ul>
                      @foreach (Session::get('success') as $msg)
                          <li>{!! $msg !!}</li>
                      @endforeach
                  </ul>
              </div>
            @endif
            @if (\Session::has('error'))
                <div class="alert alert-danger">
                    <ul>
                        @foreach (Session::get('error') as $msg)
                            <li>{!! $msg !!}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="card-header">Reportes <a href="{{route('reports.clear')}}" class="pull-right"><i class="fas fa-stream"></i></a></div>
                <div class="card-body">
                    <h4 class="mt-0 header-title">Reportes de Películas y Series</h4>
                    <div class="row">
                        <div class="col-sm-12">
                            @if(count($reports) > 0)
                            <table id="customers">
                                <tr>
                                  <th>#</th>
                                  <th>Película/Serie</th>
                                  <th>Fecha</th>
                                </tr>
                                @foreach ($reports as $report)
                                  <tr>
                                    <td>{{$report->id}}</td>
                                    <td>{{$report->movie}}</td>
                                    <td>{{$report->created_at}}</td>
                                  </tr>
                                @endforeach
                              </table>
                            @else
                            <p>Sin Reportes que mostrar</p>
                           @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection