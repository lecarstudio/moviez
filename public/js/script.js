$('.dropify').dropify({
    error: {
        'fileSize': 'El tamaño del archivo es demasiado grande ({{ value }} max).',
        'maxWidth': 'El ancho de la imagen es demasiado grande ({{ value }} px max).',
        'imageFormat': 'El formato de imagen no está permitido (sólo {{ value }}).'
    },
    messages: {
        'default': 'Arrastra y suelta un archivo aquí o haz clic',
        'replace': 'Arrastra y suelta o haz clic para reemplazar',
        'remove':  'Eliminar',
        'error':   'Ooops, Algo malo sucedió.'
    }
});