<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Config;
use App\Report;
use Storage;

class ConfigController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function update(Request $request)
    {
        try{
            $config =  Config::find(1);
            if ($request->hasFile('logo')) {
                if($config->logo != null) Storage::delete($config->logo);
                $path = $request->file('logo')->store('storage');
                $config->logo = $path;
            }
            $config->admob = $request->admob;
            $config->app_url = $request->app_url;
            $config->save();
            return redirect()->back()->with('success', ['¡Los datos se han actualizado con exito']);
        } catch (Throwable $e) {
            return redirect()->back()->with('error', ['¡Ups! A ocurrido un error inesperado, por favor intenta de nuevo o ponte en contacto con el Administrador del Sistema']);   
        }
    }

    public function report(){
        $reports = Report::all();
        return view('reports',compact('reports'));
    }

    public function clearReport(){
        try{
            Report::truncate();
            return redirect()->back()->with('success', ['¡Los reportes se han limpiado con exito']);
        } catch (Throwable $e) {
            return redirect()->back()->with('error', ['¡Ups! A ocurrido un error inesperado, por favor intenta de nuevo o ponte en contacto con el Administrador del Sistema']);   
        }
    }
}
