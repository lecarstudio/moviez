<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Notify;
use NotificationChannels\ExpoPushNotifications\ExpoChannel;
use NotificationChannels\ExpoPushNotifications\ExpoMessage;

class PushNotificationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ExpoChannel $expoChannel)
    {
        $this->middleware('auth');
        // You can quickly bootup an expo instance
        $this->expo = \ExponentPhpSDK\Expo::normalSetup();
    }
    
    public function index()
    {
        return view('push.index');
    }

    public function store(Request $request){

        $channelName = 'default';
        $recipients = Notify::all();
        if(count($recipients)>0){
            // Subscribe the recipient to the server
            foreach ($recipients as $key => $recipient) {
                $this->expo->subscribe($channelName, $recipient->value);
            }
            // Build the notification data
            if($request->url){
                $notification = [
                    'title' => $request->title,
                    'body' => $request->body,
                    'data'=> json_encode(array('url' => $request->url))
                ];
            }else{
                $notification = [
                    'title' => $request->title,
                    'body' => $request->body
                ];  
            }
           
            // Notify an interest with a notification
            $this->expo->notify([$channelName], $notification);
            return redirect()->back()->with('success', ['¡La notificación se ha enviado con exito!']);
        }else
            return redirect()->back()->with('error', ['No existen dispositivos registrados']);

       
    }
}
