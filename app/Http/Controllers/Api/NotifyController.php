<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notify;

class NotifyController extends Controller
{
    public function subscribe(Request $request){
        if(Notify::where('key', $request->key)->get()->count() == 0){
            Notify::create($request->all());
            return response([
                "status" => true,
                "message" => 'El token se ha guardado con éxito.'
            ]);
        }
        return response([
            "status" => false,
            "message" => 'El token se ha guardado previamente.'
        ]);
    }
}