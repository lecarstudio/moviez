<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Goutte\Client;

class MoviesController extends Controller
{
    public function index(Request $request){

        $client = new Client();
        $movies = collect([]);
        $crawler = $client->request('GET', $request->movie_id);

        $crawler->filter('div.Posters>a')->each(function ($movie, $index) use($movies, $crawler) {
        $movies->push(  [
                'key' => $index,
                'movie_id' => $movie->attr('href'),
                'image' => $movie->children()->filter('img')->attr('src'),
                'type' => $movie->children()->filter('.centrado')->count() > 0 ? $movie->children()->filter('.centrado')->text() : ''
            ]);
        });

        return response([
            'cover' => [
                'key' => 1,
                'movie_id' => 'https://pelisplushd.net/pelicula/aves-de-presa',
                'image' => 'https://begeeksociety.com/wp-content/uploads/2020/02/harley-quinn-aves-de-presa.jpg'
            ],
            'movies'=>$movies,
        ]);
        
    }

    public function show(Request $request){
        $client = new Client();
        $movie = collect([]);
        $crawler = $client->request('GET', $request->movie_id);

        $crawler->filter('div.m-v-30')->each(function ($node, $index) use($movie, $crawler) {
            $date = str_replace('Fecha de estreno: ','',$node->children()->filter('.sectionDetail')->eq(4)->text());
            $text = $crawler->filter('div.player')->html();
            $findme = 'https://feurl.com/';
            $pos = $pos = strpos($text, $findme);
            $url = '';
            for ($i=$pos; $i < strlen($text); $i++) { 
                if($text[$i] == "'")  break;
                $url .= $text[$i];
                   
            }
            $movie->push(  [
                'key' => $index,
                'title' =>  $node->children()->filter('h1')->text(),
                'description' => $node->children()->filter('.text-large')->text(),
                'image' => str_replace('w154','w1280',$node->children()->filter('.img-fluid')->attr('src')),
                'country' => str_replace('Pais: ','',$node->children()->filter('.sectionDetail')->eq(3)->text()),
                'premier' => $date,
                'video_id' => $url,//$crawler->filter('div#video-content>iframe')->attr('src'),
                'source' => str_replace('https://feurl.com/v/','https://feurl.com/api/source/',$url)
            ]);
        });

        return response($movie);
        
    }

    public function video(Request $request){
        $client = new Client();
        $movies = collect([]);
        $scripts = collect([]);

        $crawler = $client->request('GET', $request->serie_id);

        $crawler->filter('script')->each(function ($script, $index) use($scripts, $crawler) {
            $scripts->push(  [
               'text' => $script->html()
            ]);
        });
        

        $videoURL = strpos($scripts[6]['text'], 'https://fastplay.to/embed', 1);
        $video = '';

        for ($i=$videoURL; $i < strlen($scripts[6]['text']) ; $i++) { 
            if($scripts[6]['text'][$i] != "'"){
                $video .= $scripts[6]['text'][$i];
            }else{
                break;
            }
        }

        return response([
            'video_id' => $video
        ]);
    }
}
