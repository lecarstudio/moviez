<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Goutte\Client;

class GenereController extends Controller
{
    public function index(Request $request){

        $client = new Client();
        $generes = collect([]);
        $crawler = $client->request('GET', 'https://pelisplushd.net/');

        $crawler->filter('ul.dropdown-menu')->eq(3)->each(function ($genere, $index) use($generes, $crawler) {
            $genere->children()->filter('li')->each(function($node, $index2) use($generes, $crawler, $index) {
                $generes->push(  [
                    'key' => $index2,
                    'url' => 'https://pelisplushd.net'.$node->children()->filter('a')->attr('href'),
                    'title' => $node->children()->filter('a')->text(),
                    'icon' => 'film'
                ]);
            });
        });

        return response($generes);
        
    }

}
