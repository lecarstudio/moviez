<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Config;
use App\Report;


class ConfigController extends Controller
{
    public function index(){
        $config = Config::find(1);
        return response($config);
    }

    public function report(Request $request){

        Report::create($request->all());
        
        return response([
            "status" => true,
            "message" => 'El reporte se ha envíado con éxito.'
        ]);
    
    }
}