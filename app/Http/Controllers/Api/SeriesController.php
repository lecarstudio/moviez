<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Goutte\Client;

class SeriesController extends Controller
{
    public function index(Request $request){

        $client = new Client();
        $series = collect([]);
        $crawler = $client->request('GET', $request->serie_id);

        $crawler->filter('div.Posters>a')->each(function ($serie, $index) use($series, $crawler) {
        $series->push(  [
                'key' => $index,
                'movie_id' => $serie->attr('href'),
                'image' => $serie->children()->filter('img')->attr('src'),
            ]);
        });

        return response($series);
        
    }

    public function show(Request $request){
        $client = new Client();
        $serie = collect([]);
        $seasons = collect([]);
        $crawler = $client->request('GET', $request->serie_id);
        $crawler->filter('div.m-v-30')->each(function ($node, $index) use($serie, $crawler, $seasons) {
                $date = str_replace('Fecha de estreno: ','',$node->children()->filter('.sectionDetail')->eq(4)->text());
            for ($i=1; $i <=  $crawler->filter('li.presentation')->count() ; $i++) { 
                $ses = collect([]);
                $crawler->filter('#pills-vertical-'.$i.'>a')->each(function ($session, $index) use($crawler, $i, $ses) {
                    $ses->push([
                        'key' => $index,
                        'name' => $session->text(),
                        'season_id' => $session->attr('href')
                    ]);
                });
                $seasons->push($ses);
                $ses = collect([]);
            }
           
            $serie->push(  [
                    'key' => $index,
                    'title' =>  $node->children()->filter('h1')->text(),
                    'description' => $node->children()->filter('.text-large')->text(),
                    'image' => str_replace('w154','w1280',$node->children()->filter('.img-fluid')->attr('src')),
                    'country' => str_replace('Pais: ','',$node->children()->filter('.sectionDetail')->eq(3)->text()),
                    'premier' => $date,
                    'numSeasons' => $crawler->filter('li.presentation')->count(),
                    'seasons' => $seasons,

                ]);
        });
        return response($serie);
        
    }

    public function getUrl(Request $request){
        $client = new Client();
        $serie = collect([]);
        $crawler = $client->request('GET', $request->serie_id);

        $text = $crawler->filter('div.player')->html();
        $findme = "'https://feurl.com";
        $pos = $pos = strpos($text, $findme);
        $url = '';
        for ($i=$pos+1; $i < strlen($text); $i++) { 
            if($text[$i] == "'")  break;
            $url .= $text[$i];
               
        }
        $serie->push([
            'key' => 0,
            'video_id' => $url,
            'source' => str_replace('https://feurl.com/v/','https://feurl.com/api/source/',$url)

        ]);
        return response($serie);
    }

    public function video(Request $request){
        $client = new Client();
        $movies = collect([]);
        $scripts = collect([]);

        $crawler = $client->request('GET', $request->movie_id);

        $crawler->filter('script')->each(function ($script, $index) use($scripts, $crawler) {
            $scripts->push(  [
               'text' => $script->html()
            ]);
        });
        

        $videoURL = strpos($scripts[6]['text'], 'https://fastplay.to/embed', 1);
        $video = '';

        for ($i=$videoURL; $i < strlen($scripts[6]['text']) ; $i++) { 
            if($scripts[6]['text'][$i] != "'"){
                $video .= $scripts[6]['text'][$i];
            }else{
                break;
            }
        }

        return response([
            'video_id' => $video
        ]);
    }
}
