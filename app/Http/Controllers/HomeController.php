<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Config;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    
    public function index()
    {
        $config = Config::find(1);
        return view('home',compact('config'));
    }
}
