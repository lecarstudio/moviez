<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notify extends Model
{
    protected $table = 'exponent_push_notification_interests';

    protected $fillable = [
        'key',
        'value'
    ];
}
