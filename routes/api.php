<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::namespace('Api')->group(function () {
    Route::post('/config', 'ConfigController@index');
    Route::post('/report', 'ConfigController@report');
    Route::post('/movies', 'MoviesController@index');
    Route::post('/movies/show', 'MoviesController@show');
    Route::post('/movies/video', 'MoviesController@video');
    Route::post('/generes', 'GenereController@index');
    Route::post('/series', 'SeriesController@index');
    Route::post('/series/show', 'SeriesController@show');
    Route::post('/series/video', 'SeriesController@video');
    Route::post('/series/url', 'SeriesController@getUrl');
    Route::post('/nofity', 'NotifyController@subscribe');
});
