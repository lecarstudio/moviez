<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('home');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/report', 'ConfigController@report')->name('reports');
Route::get('/report/clear', 'ConfigController@clearReport')->name('reports.clear');
Route::post('/config', 'ConfigController@update')->name('config.update');
Route::get('/push', 'PushNotificationController@index')->name('push.index');
Route::post('/push/add', 'PushNotificationController@store')->name('push.store');

